
 ## Introduction

Geographical Information Systems(GIS) prototype for postGres spaital analysis.
   <img  src="./national-alerts.png">  
<br>
<img height="150" width="150" src="./docker-original-wordmark.svg">
<img height="150" width="150" src="./nodejs-original-wordmark.svg">
<img height="150" width="150" src="./nestjs-plain-wordmark.svg">
<img height="150" width="150" src="./graphql-plain-wordmark.svg">
 <img height="150" width="225" src="./logo_big.png">
<img height="150" width="150" src="./postgresql-original-wordmark.svg">

## Description

Investigation into the spatial abilities of the postgres database. We will load shape files from the NOAA storm predicition center into 
postgres database and execute spatial query to identify if certain coordinates are location inside a weather alert polygon. Spatial functions
can be used for 
- Mobile devices to generate alert notification if user within weather alert polygons
- IOT devices with geolocation capabilities to identify devices within weather alert polygons
- Supply Chain routing to idenify potential bottlenecks if materials are within weather alert polygons

## Installation

```bash
- clone repo
- npm install
- docker build -t postgres_gis .
- docker tag postgres_gis:latest postgres_gis:staging

```

## Running the app

```bash
- docker-compose up
- log into the docker container docker exec -i -t  alertpostgres_postgres_1 /bin/bash
- execute ../downlaod.sh which will download the shape files and load into postgres database
- log into the pg4Admin database which was deployed in docker container 
- navigate to the weather database
- run the following query select * from alerts
- navigate to https://www.spc.noaa.gov/products/wwa/ and drill down to a location which has alerts, you will see on the page
  the coordinates of the location
- run query SELECT ** FROM alerts WHERE ST_Contains(geom, ST_GeomFromText('POINT(-101.29 48.18)')) 
- run npm run start
- open http://localhost:3000/graphql in your browser
- run the following query
query{
 alerts (getAlerts: {lat:"46.92",lon:"-114.09"})
  {
    gid,
    url,
    phenom,
    event,
    geom
  }
}
```
