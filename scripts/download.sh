#!/bin/bash
mkdir -p zips
cd zips
wget --no-check-certificate -c https://tgftp.nws.noaa.gov/SL.us008001/DF.sha/DC.cap/DS.WWA/current_all.tar.gz -O - | tar -xz
shp2pgsql  -d current_all.shp alerts weather > alerts.sql
psql --host localhost port 5432 --username weatheruser --dbname weather --file alerts.sql 
