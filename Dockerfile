# Extend exiting PostreSQL 10 Debian image: https://hub.docker.com/_/postgres/
FROM postgis/postgis

MAINTAINER Richard Helferty

RUN mkdir -p /scripts
COPY ./scripts/download.sh /scripts
WORKDIR /scripts
RUN chmod +x download.sh
# Install packages
RUN apt-get update
RUN apt-get install --no-install-recommends --yes \
    postgis wget locate vim \