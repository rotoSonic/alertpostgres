import { Injectable } from '@nestjs/common';
import { AlertModel } from './alerts.model';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { AlertsArgs } from './alerts.args';


@Injectable()
export class AlertsService {
    constructor(
        @InjectRepository(AlertModel)
        private alertRepository: Repository<AlertModel>,
      ) {}

      findAll(search: string): Promise<AlertModel[]> {
        let response;
        try {
          response = this.alertRepository.query("");
          return response;
        }catch(error) {
          console.log(error)
        }
        
      }
      async findByLocationGraphql(alertsArgs: AlertsArgs): Promise<any>{
        let response;
        try {
          console.log("SELECT *  FROM alerts WHERE ST_Contains(geom, ST_GeomFromText('POINT("+alertsArgs.lat+" "+alertsArgs.lon+")'))")
          response = await this.alertRepository.query("SELECT gid, cap_id, vtec, phenom, sig, wfo, event, issuance, expiration, onset, ends, url,msg_type, prod_type,geom, st_asgeojson(geom) as jsonGeom FROM alerts WHERE ST_Contains(geom, ST_GeomFromText('POINT("+alertsArgs.lon+" "+alertsArgs.lat+")'))")
          const qlData = [];
          response.map(data =>{
            const alertModel: AlertModel = new AlertModel();
            alertModel.gid = data.gid
            alertModel.cap_id = data.cap_id;
            alertModel.ends = data.ends;
            alertModel.event = data.event;
            alertModel.expiration = data.expiration;
            alertModel.geom = data.jsongeom;
            alertModel.issuance = data.issuance;
            alertModel.msg_type = data.msg_type;
            alertModel.onset = data.onset;
            alertModel.phenom = data.phenom;
            alertModel.prod_type = data.prod_type;
            alertModel.sig = data.sig;
            alertModel.url = data.url;
            alertModel.vtec = data.vtec;
            alertModel.wfo = data.wfo;
            qlData.push(alertModel)
          })
          return qlData;
        } catch(error) {
          return error;
        }
      }
      
      async findByLocation(lat: string, lon: string): Promise<any>{
        let response;
        try {
          console.log("SELECT st_asgeojson(geom)  FROM alerts WHERE ST_Contains(geom, ST_GeomFromText('POINT("+lat+" "+lon+")'))")
          response = await this.alertRepository.query("SELECT st_asgeojson(geom), phenom, expiration, cap_id, prod_type  FROM alerts WHERE ST_Contains(geom, ST_GeomFromText('POINT("+lat+" "+lon+")'))")
          const features = [];
          response.map(data =>{
            const mapData = JSON.parse(data.st_asgeojson);
            features.push({
              type: "Feature" as const,
              properties: {
                color: "black",
                opacity: 1,
                fillColor: "white",
                fillOpacity: 1
              },
              geometry: {
                type: "MultiPolgon" as const,
                coordinates: mapData.coordinates
              }
            })
          })
          console.log(features)
          return response;
        } catch(error) {
          return error;
        }
      }

      async findOne(id: number): Promise<AlertModel> {
        let response;
        try {
          response = await this.alertRepository.findOne(id);
          console.log(response)
          return response;
        } catch (error) {
          console.log(error);
        }
        
      }

}
