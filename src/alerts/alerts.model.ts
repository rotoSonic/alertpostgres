import { Entity, Column, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn, OneToMany, PrimaryColumn } from 'typeorm';
import { ObjectType, Field } from '@nestjs/graphql';
import { Geometry} from 'geojson'
import { multipolygon ,polygon, Multipolygon } from 'graphql-geojson';
import JSON from 'graphql-type-json';

@ObjectType()
@Entity("alerts")
export class AlertModel {
    @PrimaryColumn()
    @Field()
    gid: number;

    @Field()
    @Column({ length: 254, nullable: true })
    cap_id: string;

    @Field()
    @Column({ length: 48, nullable: true })
    vtec: string;

    @Field()
    @Column({ length: 2, nullable: true })
    phenom: string;

    @Field()
    @Column({ length: 1, nullable: true })
    sig: string;

    @Field()
    @Column({ length: 4, nullable: true })
    wfo: string;

    @Field()
    @Column({ length: 4, nullable: true })
    event: string;

    @Field()
    @Column({ length: 25, nullable: true })
    issuance: string;

    @Field()
    @Column({ length: 25, nullable: true })
    expiration: string;

    @Field()
    @Column({ length: 25, nullable: true })
    onset: string;

    @Field()
    @Column({ length: 25, nullable: true })
    ends: string;

    @Field()
    @Column({ length: 254, nullable: true })
    url: string;

    @Field()
    @Column({ length: 3, nullable: true })
    msg_type: string;

    @Field()
    @Column({ length: 40, nullable: true })
    prod_type: string;
/*
    @Field(() => JSON)
    geom: any;
    */
    @Field(() => JSON)
    @Column({
        type: 'geometry',
        nullable: true,
        spatialFeatureType: 'MultiPolygon',
        srid: 4326
    })
    geom: any;










}