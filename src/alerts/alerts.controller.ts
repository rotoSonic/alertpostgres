import { Controller, Get, Param, Post, Body, Put, Delete, Inject, Query, Req} from '@nestjs/common';
import { endWith } from 'rxjs';
import { Request } from 'express';
import { AlertsService } from './alerts.service';


@Controller('alerts')
export class AlertsController {
    constructor(@Inject(AlertsService) private alertService: AlertsService) {}
    
    @Get('/?')
    findOne(@Req() request: Request): Object {
        const response = this.alertService.findByLocation(request.query.lat.toString(),request.query.lon.toString());
        console.log(typeof response)
        return response;
    }

}






