import { Module} from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AlertModel } from './alerts.model';
import { AlertsController } from './alerts.controller';
import { AlertsService } from './alerts.service';
import { AlertsResolver } from './alerts.resolver';

@Module({
  imports: [TypeOrmModule.forFeature([AlertModel])],
  controllers: [AlertsController],
  providers: [AlertsService, AlertsResolver],
  exports: [AlertsService]
})
export class AlertsModule {}
