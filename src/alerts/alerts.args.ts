
import { ArgsType, Field, InputType, ObjectType } from '@nestjs/graphql';
import { IsNotEmpty, isNotEmpty } from 'class-validator';

@InputType('getAlerts')
@ArgsType()
export class AlertsArgs {
    @Field()
    @IsNotEmpty()
    lat: string;
    @Field()
    @IsNotEmpty()
    lon: string;

}