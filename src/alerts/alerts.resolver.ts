import { AlertModel } from './alerts.model';
import { AlertsService } from './alerts.service';
import { Resolver, Mutation, Args, Query, ResolveField, Parent } from '@nestjs/graphql';
import { Inject } from '@nestjs/common';
import { AlertsArgs } from './alerts.args';


@Resolver(of => AlertModel)
export class AlertsResolver {
    constructor(
        @Inject(AlertsService) private alertService: AlertsService
      ) { }
      //createUser(@Args('createUserData') createUserData: CreateUserInput): User {
      @Query(returns => [AlertModel])
      async alerts(@Args('getAlerts') alertsArgs: AlertsArgs):Promise<any>  {
        let test = await this.alertService.findByLocationGraphql(alertsArgs);
        return test;
        //return await this.alertService.findByLocationGraphql(alertsArgs);
      }

}
