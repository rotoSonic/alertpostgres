CREATE DATABASE weather;
CREATE USER weatherUser WITH PASSWORD 'weatherPassword';
CREATE EXTENSION postgis;
CREATE EXTENSION postgis_topology;